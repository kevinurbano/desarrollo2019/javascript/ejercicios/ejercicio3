Algoritmo Ejercicio10
	Escribir 'Introduce Nota'
	Leer nota
	resultado <- ''
	Si nota>=5 Entonces
		Si nota>=6 Entonces
			Si nota>=7 Entonces
				Si nota>=9 Entonces
					Si nota>10 Entonces
						resultado <- 'Numero no valido'
					SiNo
						resultado <- 'Sobresaliente'
					FinSi
				SiNo
					resultado <- 'Notable'
				FinSi
			SiNo
				resultado <- 'Bien'
			FinSi
		SiNo
			resultado <- 'Suficiente'
		FinSi
	SiNo
		resultado <- 'Suspenso'
	FinSi
	Escribir resultado
FinAlgoritmo

